package com.example.handycatch;

import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CreerProfile extends AppCompatActivity {
    private EditText Nom;
    private EditText Prenom;

    private Button create_button;
    private Switch isAvailable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creer_profile_layout);

        Nom = (EditText) findViewById(R.id.edtItem);
        Prenom = (EditText) findViewById(R.id.edtAdditionalInformation);

        create_button = findViewById(R.id.btnCreate);
        isAvailable = (Switch) findViewById(R.id.swiAvailable);
        // Get Date from CalendarView



        create_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Validating the log in data
                boolean validationError = false;

                StringBuilder validationErrorMessage = new StringBuilder("Please, ");
                if (isEmptyText(Nom)) {
                    validationError = true;
                    validationErrorMessage.append("insert an name");
                }
                validationErrorMessage.append(".");
                if (validationError) {
                    Toast.makeText(CreerProfile.this, validationErrorMessage.toString(), Toast.LENGTH_LONG).show();
                    return;
                } else {
                    saveObject();
                }
            }
        });
    }

    private boolean isEmptyText(EditText text) {
        if (text.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public void saveObject(){
        // Configure Query
        //ParseObject User = new ParseObject("User");
        ParseUser currentUser = ParseUser.getCurrentUser();
        // Store an object
        currentUser.put("nom", Nom.getText().toString());
        currentUser.put("prenom", Prenom.getText().toString());

        //User.put("username", ParseUser.getCurrentUser());

        // Saving object
        currentUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Intent intent = new Intent(CreerProfile.this, Profil.class);
                    startActivity(intent);
                    //Toast.makeText(CreerProfile.this, "C'est bon", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            e.getMessage().toString(),
                            Toast.LENGTH_LONG
                    ).show();
                }
            }
        });
    }
}