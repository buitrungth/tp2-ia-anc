package com.example.handycatch;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class Profil extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profil_layout);
        final Button button_creer = findViewById(R.id.button_creer);
        button_creer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent creerProfile = new Intent(Profil.this , CreerProfile.class);
                startActivity(creerProfile);

            }
        });
    }
}
